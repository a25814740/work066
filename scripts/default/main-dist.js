'use strict';

var window = $(window),
    layoutHeader = $('.layui-header'),
    layoutBody = $('.layui-body'),
    layoutFooter = $('.layui-footer');

$(function () {
    layuiFooterExist();
});
$(window).on('resize', function () {
    setTimeout(function () {
        layuiFooterExist();
    }, 10);
});

function layuiFooterExist() {
    // var layoutHeaderH = layoutHeader.outerHeight(),
    //     layoutFooterH = layoutFooter.outerHeight(),
    //     layoutBodyH = $(window).height() - layoutHeaderH - layoutFooterH;
    // if ($(window).width() > 992 ) {
    //     layoutBody.css('height', layoutBodyH);
    // } else {
    //     layoutBody.css('height', 'auto');
    //     return false;
    // }
    if ($('.layui-footer').length > 0) {
        $('body').addClass('hasLayuiFooter');
    } else {
        $('body').removeClass('hasLayuiFooter');
    }
}

function addColumn(event) {
    var _target = event;
    console.log(_target);
    var insetElement = $(_target).parents('[data-add-wrapper]');
    var template = '\n    <div class="form-group-group col-12 row no-gutters p-0" data-add-template="formAdd">\n        <div class="col-6 row no-gutters px-2 py-2 align-items-center">\n            <div class="col row no-gutters px-2 py-0 align-items-center">\n                <div class="col-auto"><span class="no"></span></div>\n                <div class="col">\n                    <input class="form-control" id="form-group7" type="text">\n                </div>\n            </div>\n        </div>\n        <div class="col-6 row no-gutters px-2 py-2 align-items-center">\n            <div class="col">\n                <input class="form-control text-right" id="form-group7-2" type="text">\n            </div>\n            <div class="col-auto"><span>\u516C\u514B </span></div>\n        </div>\n    </div>\n    ';
    insetElement.append(template);

    var _addTemplate = $(_target).parents('[data-add-wrapper]').find('[data-add-template="formAdd"]');
    console.log(_addTemplate.length);
    for (var i = 0; i < _addTemplate.length; i++) {
        var ii = i + 1;
        var addTemplate = _addTemplate.eq(i);
        addTemplate.find('.no').text(ii + '.');
    }
}